#!/usr/bin/env python3
# coding: utf-8

import pylab as p
import sys, configparser
from string import ascii_letters as letters

config = configparser.ConfigParser()
try:
    configfile = sys.argv[1]
except IndexError:
    configfile = 'default.ini'
config.read(configfile)
data = config['DATA']

NB_PLANCHES = int(data['NB_PLANCHES'])
DIAMETRE_A = float(data['DIAMETRE_A'])
DIAMETRE_B = float(data['DIAMETRE_B'])
EPAISSEUR_PLANCHE = float(data['EPAISSEUR_PLANCHE'])
LONGUEUR_PLANCHE = float(data['LONGUEUR_PLANCHE'])
COEFF_SECURITE = float(data['COEFF_SECURITE'])

points_interieurs_x = []
points_interieurs_y = []

points_exterieurs_x = []
points_exterieurs_y = []
for i in range(NB_PLANCHES+1):
    rayon_interieur_a = DIAMETRE_A/2
    rayon_interieur_b = DIAMETRE_B/2
    rayon_exterieur_a = rayon_interieur_a + EPAISSEUR_PLANCHE
    rayon_exterieur_b = rayon_interieur_b + EPAISSEUR_PLANCHE
    angle = p.pi/NB_PLANCHES

    points_interieurs_x.append(rayon_interieur_a*p.cos(i*angle))
    points_interieurs_y.append(-rayon_interieur_b*p.sin(i*angle))
    points_exterieurs_x.append(rayon_exterieur_a*p.cos(i*angle))
    points_exterieurs_y.append(-rayon_exterieur_b*p.sin(i*angle))

ax = p.subplot(221)

for i in range(NB_PLANCHES):
#
#   D +-------+ A
#    /|       |\
#   / | h     | \
#  +--+-------+--+ B
#  C  t2      t1

    a_x = points_interieurs_x[i + 1]
    a_y = points_interieurs_y[i + 1]
    b_x = points_exterieurs_x[i + 1]
    b_y = points_exterieurs_y[i + 1]
    c_x = points_exterieurs_x[i]
    c_y = points_exterieurs_y[i]
    d_x = points_interieurs_x[i]
    d_y = points_interieurs_y[i]

    h = EPAISSEUR_PLANCHE

    cb = p.array([c_x - b_x, c_y - b_y])
    da = p.array([d_x - a_x, d_y - a_y])

    n_cb = p.norm(cb)
    n_da = p.norm(da)

    I1 = p.array([b_x, b_y]) + cb/2
    I2 = p.array([a_x, a_y]) + da/2
    CG = (I1 + I2)/2 + (I1 - I2) * h/3 * (n_da + 2 * n_cb)/(n_da + n_cb)
    p.text(CG[0], CG[1], letters[i])


    p.plot([a_x, d_x], [a_y, d_y], c='C2')
    p.plot([c_x, b_x], [c_y, b_y], c='C3')
    p.plot([a_x, b_x], [a_y, b_y], c='C1')
    p.plot([c_x, d_x], [c_y, d_y], c='C1')

p.axis('equal')
p.grid()

p.subplot(223, sharex=ax)

for i in range(NB_PLANCHES+1):
    x = [points_interieurs_x[i], points_interieurs_x[i]]
    y = [0, LONGUEUR_PLANCHE]
    p.plot(x, y, '-', c='C2')

    x = [points_exterieurs_x[i], points_exterieurs_x[i]]
    p.plot(x, y, '--', c='C3')

p.axis('equal')
p.grid()

p.subplot(222, sharey=ax)
for i in range(NB_PLANCHES+1):
    x = [-DIAMETRE_A/2, DIAMETRE_A/2]
    y = [points_interieurs_y[i], points_interieurs_y[i]]
    p.plot(x, y, '-', c='C2')

    y = [points_exterieurs_y[i], points_exterieurs_y[i]]
    p.plot(x, y, '--', c='C3')

p.axis('equal')
p.grid()

print('Nécessaire :')
for i in range(NB_PLANCHES):
#
#   D +-------+ A
#    /|       |\
#   / | h     | \
#  +--+-------+--+ B
#  C  t2      t1

    a_x = points_interieurs_x[i + 1]
    a_y = points_interieurs_y[i + 1]
    b_x = points_exterieurs_x[i + 1]
    b_y = points_exterieurs_y[i + 1]
    c_x = points_exterieurs_x[i]
    c_y = points_exterieurs_y[i]
    d_x = points_interieurs_x[i]
    d_y = points_interieurs_y[i]

    h = EPAISSEUR_PLANCHE

    ab = p.array([b_x - a_x, b_y - a_y])
    bc = p.array([c_x - b_x, c_y - b_y])
    angle_abc = p.arccos(ab.dot(bc)/ (p.norm(ab) * p.norm(bc)))

    bc = p.array([c_x - b_x, c_y - b_y])
    cd = p.array([d_x - c_x, d_y - c_y])
    angle_bcd = p.arccos(bc.dot(cd)/ (p.norm(bc) * p.norm(cd)))

    bt1 = abs(p.cos(angle_abc)*p.norm(ab))
    ct2 = abs(p.cos(angle_bcd)*p.norm(cd))

    print('\t - {}) 1 planche de {:0.1f}cm × {:0.1f}cm x {:0.1f}cm'.format(letters[i], LONGUEUR_PLANCHE*100, p.norm(bc)*100, h*100))
    print('\t\t à rongner : {:0.1f}cm et {:0.1f}cm'.format(100*ct2, 100*bt1))

text = '\t - 2 planches de {:0.1f}cm × {:0.1f}cm × {:0.1f}cm'
print(text.format(100*COEFF_SECURITE*(DIAMETRE_A + 2*EPAISSEUR_PLANCHE),
                  100*COEFF_SECURITE*(DIAMETRE_B/2 + EPAISSEUR_PLANCHE),
                  100*EPAISSEUR_PLANCHE))

p.show()
